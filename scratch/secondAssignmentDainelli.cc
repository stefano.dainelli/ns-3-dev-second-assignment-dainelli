#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ssid.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/single-model-spectrum-channel.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/netanim-module.h"

#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/non-communicating-net-device.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/string.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/wifi-net-device.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE("SecondAssignment"); 

Ptr<SpectrumModel> SpectrumModelWifi5180MHz; // Spectrum model at 5180 MHz.

// Initializer for a static spectrum model centered around 5180 MHz
class static_SpectrumModelWifi5180MHz_initializer
{
  public:
    static_SpectrumModelWifi5180MHz_initializer()
    {
        BandInfo bandInfo;
        bandInfo.fc = 5180e6;
        bandInfo.fl = 5180e6 - 10e6;
        bandInfo.fh = 5180e6 + 10e6;

        Bands bands;
        bands.push_back(bandInfo);

        SpectrumModelWifi5180MHz = Create<SpectrumModel>(bands);
    }
};

/// Static instance to initizlize the spectrum model around 5180 MHz.
static_SpectrumModelWifi5180MHz_initializer static_SpectrumModelWifi5180MHz_initializer_instance;

int
main(int argc, char* argv[])
{

    bool verbose = true;
    uint32_t nWifi = 3; //Creo 3 nodi STA
    bool tracing = false;
    bool pcap = true; // true --> stampa pcap, false --> non stampa
    bool protocol = false; //true --> UDP, false --> TCP
    //Scelta WifiType
    //std::string wifiType = "ns3::SpectrumWifiPhy";
    std::string wifiType = "ns3::YansWifiPhy";

    

    std::string errorModelType = "ns3::NistErrorRateModel";
    uint16_t frequency = 5180;

    CommandLine cmd(__FILE__);
    cmd.AddValue("nWifi", "Number of wifi STA devices", nWifi);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
    cmd.AddValue("tracing", "Enable pcap tracing", tracing);
    cmd.AddValue("wifiType", "select ns3::SpectrumWifiPhy or ns3::YansWifiPhy", wifiType);
    cmd.AddValue("errorModelType",
                 "select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
                 errorModelType);
    cmd.AddValue("udp", "UDP if set to 1, TCP otherwise", protocol);

    cmd.Parse(argc, argv);

    // The underlying restriction of 18 is due to the grid position allocator's configuration; the grid layout will exceed the bounding box if more than 18 nodes are provided.
    if (nWifi > 18)
    {
        std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box"
                  << std::endl;
        return 1;
    }

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

    uint32_t payloadSize;
    if (protocol)
    {
        payloadSize = 862; // 1000 bytes IPv4
    }
    else
    {
        payloadSize = 1568; // 1500 bytes IPv6
        Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));
    }

    // Creazione nodi STA e AP
    NodeContainer sta1_wifi, sta2_wifi;
    sta1_wifi.Create(nWifi);
    sta2_wifi.Create(nWifi);

    NodeContainer ap_wifi;
    ap_wifi.Create(2);

    YansWifiPhyHelper yansPhy;
    SpectrumWifiPhyHelper spectrumPhy;
    Ptr<MultiModelSpectrumChannel> spectrumChannel1, spectrumChannel2;

//####################################################################################### SEZIONE CANALI
    if (wifiType == "ns3::YansWifiPhy") {
        //Canali YANS
        //Canale 1
        YansWifiChannelHelper channel1;
        //we create a channel object and associate it to our PHY layer object manager 
        channel1.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                    "Frequency",
                                    DoubleValue(frequency * 1e6));
        channel1.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");  
        yansPhy.SetChannel(channel1.Create());
        yansPhy.Set("ChannelSettings", StringValue(std::string("{36, 0, BAND_5GHZ, 0}")));
        
        //Canale 2
        YansWifiChannelHelper channel2;
        channel2.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                    "Frequency",
                                    DoubleValue(frequency * 1e6));
        channel2.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
        yansPhy.SetChannel(channel2.Create());
        yansPhy.Set("ChannelSettings", StringValue(std::string("{38, 0, BAND_5GHZ, 0}")));
    }
    else if (wifiType == "ns3::SpectrumWifiPhy") {
        //Canali SPECTRUM
        //Canale 1
        spectrumChannel1 = CreateObject<MultiModelSpectrumChannel>();
        Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
        lossModel->SetFrequency(frequency * 1e6);
        spectrumChannel1->AddPropagationLossModel(lossModel);

        Ptr<ConstantSpeedPropagationDelayModel> delayModel =
            CreateObject<ConstantSpeedPropagationDelayModel>();
        spectrumChannel1->SetPropagationDelayModel(delayModel);

        spectrumPhy.SetChannel(spectrumChannel1);
        spectrumPhy.SetErrorRateModel(errorModelType);
        spectrumPhy.Set("ChannelSettings", StringValue(std::string("{36, 0, BAND_5GHZ, 0}")));

        //Canale 2
        spectrumChannel2 = CreateObject<MultiModelSpectrumChannel>();
        lossModel->SetFrequency(frequency * 1e6);
        spectrumChannel2->AddPropagationLossModel(lossModel);

        spectrumChannel2->SetPropagationDelayModel(delayModel);

        spectrumPhy.SetChannel(spectrumChannel2);
        spectrumPhy.SetErrorRateModel(errorModelType);
        spectrumPhy.Set("ChannelSettings", StringValue(std::string("{38, 0, BAND_5GHZ, 0}")));

    }
    else
    {
        NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
    }

//####################################################################################### MAC
    //WifiMacHelper object is used to set MAC parameters.    
    WifiMacHelper mac; //MAC prima rete e seconda rete

    //SSID reti
    Ssid ssid = Ssid("wifi1-80211n");
    Ssid ssid2 = Ssid("wifi2-80211n");

    //WifiHelper --> imposta standard 802.11n
    WifiHelper wifi1, wifi2; 
    wifi1.SetStandard(WIFI_STANDARD_80211n); 
    wifi2.SetStandard(WIFI_STANDARD_80211n);

    //Creazione Container
    NetDeviceContainer staDevices1, staDevices2;
    NetDeviceContainer apDevices1, apDevices2;

    //YANS
    if (wifiType == "ns3::YansWifiPhy")
    {
        //STA 1 e 2
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid), "ActiveProbing", BooleanValue(false));
        staDevices1 = wifi1.Install(yansPhy, mac, sta1_wifi);

        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2), "ActiveProbing", BooleanValue(false));
        staDevices2 = wifi2.Install(yansPhy, mac, sta2_wifi);

        //AP 1 e 2
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid)); 
        apDevices1 = wifi1.Install(yansPhy, mac, ap_wifi.Get(0));

        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2)); 
        apDevices2 = wifi1.Install(yansPhy, mac, ap_wifi.Get(1));
    }

    //SPECTRUM
    else if (wifiType == "ns3::SpectrumWifiPhy")
    {
        //STA 1 e 2
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid), "ActiveProbing", BooleanValue(true));
        staDevices1 = wifi1.Install(spectrumPhy, mac, sta1_wifi);

        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2), "ActiveProbing", BooleanValue(true));
        staDevices2 = wifi2.Install(spectrumPhy, mac, sta2_wifi);

        //AP 1 e 2
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid)); 
        apDevices1 = wifi1.Install(spectrumPhy, mac, ap_wifi.Get(0));

        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2)); 
        apDevices2 = wifi1.Install(spectrumPhy, mac, ap_wifi.Get(1));
    }

//####################################################################################### MOBILITY
    MobilityHelper mobility;

    //Disposizione nodi in colonna
    // STA1                 STA4
    // STA2     AP1-2       STA5
    // STA3                 STA6

    //STA prima colonna
    mobility.SetPositionAllocator("ns3::GridPositionAllocator", "MinX", DoubleValue(0.0), "MinY", DoubleValue(0.0), "DeltaX", DoubleValue(0.0),
                                "DeltaY", DoubleValue(3.0), "GridWidth", UintegerValue(3), "LayoutType", StringValue("ColumnFirst")); //We have arranged our nodes on an initial grid

    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel"); //fa in modo che i nodi siano fissi
        
    mobility.Install(sta1_wifi); //install the mobility models on the STA nodes.

    //AP sono sullo stesso punto al centro                     
    mobility.SetPositionAllocator("ns3::GridPositionAllocator", "MinX", DoubleValue(3.0), "MinY", DoubleValue(3.0), "DeltaX", DoubleValue(0.0),
                                "DeltaY",DoubleValue(0.0),"GridWidth", UintegerValue(3), "LayoutType", StringValue("RowFirst")); //We have arranged our nodes on an initial grid

    mobility.Install(ap_wifi);

    //STA seconda colona
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                "MinX", DoubleValue(6.0), "MinY", DoubleValue(0.0), "DeltaX", DoubleValue(0.0), "DeltaY", DoubleValue(3.0),
                                "GridWidth", UintegerValue(3), "LayoutType", StringValue("ColumnFirst")); //We have arranged our nodes on an initial grid

    mobility.Install(sta2_wifi); //install the mobility models on the STA nodes.

//####################################################################################### STACK & IPV4
    //install Internet stack
    InternetStackHelper stack;

    //Ipv4StaticRoutingHelper per il router statico nodi
    Ipv4StaticRoutingHelper staticRoutingHelper;
    stack.SetRoutingHelper (staticRoutingHelper);
    stack.Install(ap_wifi);
    stack.Install(sta1_wifi);
    stack.Install(sta2_wifi);  

    Ipv4AddressHelper address1, address2;

    address1.SetBase("12.10.1.0", "255.255.255.0");
    address2.SetBase("12.10.2.0", "255.255.255.0");

    //Interfaccia AP
    Ipv4InterfaceContainer wifiAPInterfaces;
    wifiAPInterfaces = address1.Assign(apDevices1);

    //Interfaccia STA
    Ipv4InterfaceContainer wifiSTAInterfaces1;
    wifiSTAInterfaces1 = address1.Assign(staDevices1);

    NodeContainer::Iterator iter;
    for (iter = sta1_wifi.Begin (); iter != sta1_wifi.End (); iter++)
        {
        Ptr<Ipv4StaticRouting> staticRouting;
        staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> ((*iter)->GetObject<Ipv4> ()->GetRoutingProtocol ());
        staticRouting->SetDefaultRoute ("<address of the AP>", 1 );
        }

    //Interfaccia AP
    Ipv4InterfaceContainer wifiAPInterfaces2;
    wifiAPInterfaces2 = address2.Assign(apDevices2);

    //Interfaccia STA
    Ipv4InterfaceContainer wifiSTAInterfaces2;
    wifiSTAInterfaces2 = address2.Assign(staDevices2);

    NodeContainer::Iterator iter2;
    for (iter2 = sta2_wifi.Begin (); iter2 != sta2_wifi.End (); iter2++)
        {
        Ptr<Ipv4StaticRouting> staticRouting2;
        staticRouting2 = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> ((*iter2)->GetObject<Ipv4> ()->GetRoutingProtocol ());
        staticRouting2->SetDefaultRoute ("<address of the AP>", 2 );
        }

    /* Setting applications */
    ApplicationContainer serverApps1, serverApps2;
//####################################################################################### Protocollo UDP
    if(protocol){
        uint16_t port1, port2;
        port1 = 9;
        port2 = 70;
    //-------------------------------------------------------------- Prima Rete
        //Server: STA 1
        UdpEchoServerHelper echoServer(port1);
        serverApps1 = echoServer.Install(sta1_wifi.Get(0));
        serverApps1.Start(Seconds(1.0));
        serverApps1.Stop(Seconds(10.0));

        //Client: STA 2 --> server: STA 1
        UdpEchoClientHelper echoClient1(wifiSTAInterfaces1.GetAddress(0), port1);
        echoClient1.SetAttribute("MaxPackets", UintegerValue(100));
        echoClient1.SetAttribute("Interval", TimeValue(Seconds(0.5))); //packet/s
        echoClient1.SetAttribute("PacketSize", UintegerValue(1024)); //playload size

        ApplicationContainer clientApps1 = echoClient1.Install(sta1_wifi.Get(1));
        clientApps1.Start(Seconds(2.0));
        clientApps1.Stop(Seconds(10.0));

        //Client: STA 3--> server: STA 1
        UdpEchoClientHelper echoClient2(wifiSTAInterfaces1.GetAddress(0), port1);
        echoClient2.SetAttribute("MaxPackets", UintegerValue(100));
        echoClient2.SetAttribute("Interval", TimeValue(Seconds(0.5))); //packet/s
        echoClient2.SetAttribute("PacketSize", UintegerValue(1024)); //playload size

        ApplicationContainer clientApps2 = echoClient2.Install(sta1_wifi.Get(2));
        clientApps2.Start(Seconds(3.0));
        clientApps2.Stop(Seconds(10.0));

    //-------------------------------------------------------------- Seconda Rete
        //Server: STA 1
        UdpEchoServerHelper echoServer2(port2);
        serverApps2 = echoServer2.Install(sta2_wifi.Get(0));
        serverApps2.Start(Seconds(1.0));
        serverApps2.Stop(Seconds(10.0));

        //Client: STA 2 --> server: STA 1
        UdpEchoClientHelper echoClient11(wifiSTAInterfaces2.GetAddress(0), port2);
        echoClient11.SetAttribute("MaxPackets", UintegerValue(100));
        echoClient11.SetAttribute("Interval", TimeValue(Seconds(1.5))); //packet/s
        echoClient11.SetAttribute("PacketSize", UintegerValue(1024)); //playload size

        ApplicationContainer clientApps11 = echoClient11.Install(sta2_wifi.Get(1));
        clientApps11.Start(Seconds(4.0));
        clientApps11.Stop(Seconds(10.0));

        //Client: STA 3 --> server: STA 1
        UdpEchoClientHelper echoClient22(wifiSTAInterfaces2.GetAddress(0), port2);
        echoClient22.SetAttribute("MaxPackets", UintegerValue(100));
        echoClient22.SetAttribute("Interval", TimeValue(Seconds(1.5))); //packet/s
        echoClient22.SetAttribute("PacketSize", UintegerValue(1024)); //playload size

        ApplicationContainer clientApps22 = echoClient22.Install(sta2_wifi.Get(2));
        clientApps22.Start(Seconds(5.0));
        clientApps22.Stop(Seconds(10.0));
    }
    else {
//####################################################################################### Protocollo TCP 
        uint16_t port1, port2;
        port1 = 10;
        port2 = 80;
        
    //--------------------------------------------------------------Prima Rete
        // Server 1
        Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port1));
        PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
        serverApps1 = packetSinkHelper.Install(sta1_wifi.Get(0));
        serverApps1.Start(Seconds(0.0));
        serverApps1.Stop(Seconds(10));

        // Client 1
        OnOffHelper onoff1("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff1.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff1.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff1.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff1.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress1(InetSocketAddress(wifiSTAInterfaces1.GetAddress(0), port1));
        onoff1.SetAttribute("Remote", remoteAddress1);
        
        ApplicationContainer clientApp1 = onoff1.Install(sta1_wifi.Get(1));
        clientApp1.Start(Seconds(1.0));
        clientApp1.Stop(Seconds(10));

        // Client 2
        OnOffHelper onoff2("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff2.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff2.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff2.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff2.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress2(InetSocketAddress(wifiSTAInterfaces1.GetAddress(0), port1));
        onoff2.SetAttribute("Remote", remoteAddress2);
        
        ApplicationContainer clientApp2 = onoff2.Install(sta1_wifi.Get(2));
        clientApp2.Start(Seconds(1.0));
        clientApp2.Stop(Seconds(10));

    //--------------------------------------------------------------Seconda Rete
        // Server 1
        Address localAddress2(InetSocketAddress(Ipv4Address::GetAny(), port2));
        PacketSinkHelper packetSinkHelper2("ns3::TcpSocketFactory", localAddress2);
        serverApps2 = packetSinkHelper2.Install(sta2_wifi.Get(0));
        serverApps2.Start(Seconds(0.0));
        serverApps2.Stop(Seconds(10));

        // Client 1
        OnOffHelper onoff11("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff11.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff11.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff11.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff11.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress11(InetSocketAddress(wifiSTAInterfaces2.GetAddress(0), port2));
        onoff11.SetAttribute("Remote", remoteAddress11);
        
        ApplicationContainer clientApp11 = onoff11.Install(sta2_wifi.Get(1));
        clientApp11.Start(Seconds(1.0));
        clientApp11.Stop(Seconds(10));

        //Client 2
        OnOffHelper onoff22("ns3::TcpSocketFactory", Ipv4Address::GetAny());
        onoff22.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        onoff22.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        onoff22.SetAttribute("PacketSize", UintegerValue(payloadSize));
        onoff22.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
        AddressValue remoteAddress22(InetSocketAddress(wifiSTAInterfaces2.GetAddress(0), port2));
        onoff22.SetAttribute("Remote", remoteAddress22);

        ApplicationContainer clientApp22 = onoff22.Install(sta2_wifi.Get(2));
        clientApp22.Start(Seconds(1.0));
        clientApp22.Stop(Seconds(10));
    }

    //Since we have built an internetwork here, we need to enable internetwork routing
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

//####################################################################################### Creazione dei Pcap
    if (wifiType == "ns3::YansWifiPhy") { //YANS
        if (pcap){
            std::stringstream ss;
            ss << "YansWifi";

            yansPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            yansPhy.EnablePcap(ss.str(), apDevices1); 

            yansPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            yansPhy.EnablePcap(ss.str(), apDevices2); 
        }
    }
    else if (wifiType == "ns3::SpectrumWifiPhy") { //SPECTRUM
        if (pcap){
            std::stringstream ss;
            ss << "MultiModelSpectrumWifi";

            spectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            spectrumPhy.EnablePcap(ss.str(), apDevices1); 

            spectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            spectrumPhy.EnablePcap(ss.str(), apDevices2);
        }
    }

//####################################################################################### Flow monitor
    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();

    Simulator::Stop(Seconds(10.0));
    
    if(protocol) {
        AnimationInterface anim("fileNetAnim.xml");
    }
    else {
        AnimationInterface anim("fileNetAnim.xml");
        anim.SetMaxPktsPerTraceFile(50000);
    }

    AnimationInterface anim("fileNetAnim.xml");

    Simulator::Run();    

    flowMonitor->SerializeToXmlFile("fileMonitor.xml", true, true);

    flowMonitor->CheckForLostPackets (); 
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
    {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

        NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
            NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
            NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
            NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");
    }

    Simulator::Destroy();

    return 0;
}